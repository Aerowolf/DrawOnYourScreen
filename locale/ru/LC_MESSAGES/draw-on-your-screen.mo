��    V      �     |      x     y  �   �     S     Y     r     �  �   �     Q	  !   ^	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     +
     K
     c
  0   h
     �
     �
     �
     �
     �
     �
        
        "     '  	   4     >     E     Y     n     �     �     �     �  
   �     �     �     �  
   �  *        /     ;  	   T     ^     c     y     �  
   �     �     �     �     �     �     �               $     0  	   ?     I     P  (   h     �  R   �     �     �                    =     I     h     m     �  
   �  5   �  �  �  $   �  L  �     8  1   N     �  :   �  S  �     /  $   O  (   t  ,   �  (   �  0   �     $     @     I     ]     r  .   �  @   �  :     2   =     p  N   �  -   �     �  4     )   @     j  $   y  ,   �     �     �  !   �          .  #   C  ,   g  5   �  A   �          !  1   2     d  
   x     �     �     �  T   �       .   )     X     s  ,   �     �  (   �     �  +   �     )     <     \     v     �     �  +   �     �  -        D     d  .   s  I   �  !   �  \        k     r  
   �  6   �  C   �  :     1   B     t  .   �  =   �     �  U        $   -   L         D              M      +   )           ;   9                 0       U              !   3                                    	       2          5           Q   R               F   <   #   E         .      &   T                    A       J      I   '   N   V              7   (       >            ?         P   
          6           "       %   /       K   1   8           ,          C   G   B   4   S               H   =                     @   O   :   *    (in drawing mode) <u>Note</u>: When you save elements made with <b>eraser</b> in a <b>SVG</b> file,
they are colored with background color, transparent if it is disabled.
(See “%s” or edit the SVG file afterwards) About Add a drawing background Area screenshot Area screenshot to clipboard By pressing <b>Ctrl</b> key <b>during</b> the drawing process, you can:
 . rotate a rectangle or a text area
 . extend and rotate an ellipse
 . curve a line (cubic Bezier curve) Center click Change font family (generic name) Change font style Change font weight Change linecap Change linejoin Change the style Color Ctrl key Dashed Dashed line Decrement line width Decrement line width even more Disable on-screen notifications Disable panel indicator Draw Draw On Your Screen becomes Draw On Your Desktop Drawing on the desktop Ellipse Enter/leave drawing mode Entering drawing mode Erase Erase all drawings Erase last brushstroke Escape key Fill Free drawing Full line Global Hide panel and dock Increment line width Increment line width even more Increment/decrement line width Internal Leave Leaving drawing mode Left click Line Menu Open stylesheet.css Persistent Persistent drawing through session restart Preferences Press Ctrl + F1 for help Rectangle Redo Redo last brushstroke Right click Save drawing as a SVG file Screenshot Screenshot to clipboard Scroll See stylesheet.css Select color Select ellipse Select eraser Select line Select rectangle Select text Shift key held Show help Smooth Smooth last brushstroke Smooth stroke during the drawing process Square drawing area Start drawing with Super+Alt+D and save your beautiful work by taking a screenshot Stroke System Text Toggle fill/stroke Transform shape (when drawing) Translators Type your text
and press Enter Undo Undo last brushstroke Unselect shape (free drawing) Version %d You can also smooth the stroke afterward
See “%s” Project-Id-Version: Draw On Your Screen VERSION
Report-Msgid-Bugs-To: https://framagit.org/abakkk/DrawOnYourScreen/issues
PO-Revision-Date: 2019-10-17 17:54+0300
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
Last-Translator: Andrew Sazonow <sazonow1980.@gmail.com>
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
Language: ru_RU
 (в режиме рисования) <u>Примечание</u>: При сохранении элементов, сделанные с помощью <b>ластика</b> в <b>SVG</b> файл,
они окрашены в цвет фона, прозрачный, если он отключен.
(Смотрите “%s” или редактируйте файл SVG) О программе Добавить фон для рисования Скриншот области Скриншот области в буфер обмена Нажатием <b>Ctrl</b> клавиши <b>в течение</b> процесс рисования, вы можете:
 . повернуть прямоугольник или текстовой области
 . расширить и вращать эллипс
 . кривая линия (кубическая кривая Безье) Центральный клик Изменять имя шрифта Изменить стиль шрифта Изменение размер шрифта Изменить линию крышки Изменить линию соединения Изменить стиль Цвет Клавиша Ctrl Пунктирная Пунктирная линия Ширина линии уменьшается Уменьшение ширины линии еще больше Отключить экранные уведомления Отключить индикатор панели Рисовать Draw On Your Screen будет рисовать на рабочем столе Рисунок на рабочем столе Эллипс Вход/выход в режим рисования Вход в режим рисования Стереть Стереть все рисунки Стереть последний мазок Клавиша Escape Заполнить Свободный рисунок Полная линия Глобальные Скрыть панель и док Приращение ширины линии Приращение ширины даже более Увеличение/уменьшение ширины линии Внутренний Покинуть Выходя из режима рисования Левый клик Линия Меню Открыть stylesheet.css Настойчивый Постоянное рисование через перезапуск сеанса Настройки Нажмите Ctrl + F1 для справки Прямоугольник Повтор Повтор последнего мазка Правый клик Сохранить рисунок в SVG Скриншот Скриншот в буфер обмена Прокрутка Смотрите stylesheet.css Выберите цвет Выберите эллипс Выберите ластик Выберите линии Выберите прямоугольник Выберите текст Клавиша Shift удерживается Показать справку Гладкая Гладкая последнего мазка Плавный ход во время процесса рисования Площадь рисования Начните рисовать Super+Alt+D и сохранить, взяв скриншот Ход Система Текст Переключение заливки/обводки Преобразовать фигуру (при рисовании) <a href="mailto:sazonow1980.@gmail.com">Andrew Sazonow</a> Введите текст
и нажмите Enter Отменить Отменить последний мазок Снимите форму (свободный рисунок) Версия %d Вы также можете сгладить ход позже
Видеть “%s” 