��    W      �     �      �     �  �   �     c     i     �     �  �   �     a	  !   n	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     
     
     '
     F
     f
     ~
  0   �
     �
     �
     �
     �
                 
   2     =     B  	   O     Y     `     t     �     �     �     �     �  
   �     �     �        
     *        J     V  	   o     y     ~     �     �  
   �     �     �     �     �               "     .     ?     K  	   Z     d     k  (   �     �  R   �               !     &     9     X     d     �     �     �  
   �  5   �  �  �     �  �   �     �     �     �     �  �        �  +   �               8     Q     a     q  
   x  
   �     �     �  #   �  2   �  %     "   +     N  0   V     �     �  "   �     �     �     �       	   !     +     2     A     M     U  #   n  1   �  +   �     �     �     �          ,     2     7     K  8   W  
   �     �  
   �     �     �     �     �  
   
          .     5     I     Z     n     ~     �     �     �     �     �     �  +         ,  `   E     �     �     �  '   �  &   �     
  #        A     I  "   e     �  3   �     %   .   M         E       !       N      ,   *           <   :                 1       V              "   4                                    	       3          6          R   S               G   =   $   F         /      '   U                    B       K      J   (   O   W              8   )       ?            @         Q   
          7           #       &   0       L   2   9           -          D   H   C   5   T               I   >                     A   P   ;   +    (in drawing mode) <u>Note</u>: When you save elements made with <b>eraser</b> in a <b>SVG</b> file,
they are colored with background color, transparent if it is disabled.
(See “%s” or edit the SVG file afterwards) About Add a drawing background Area screenshot Area screenshot to clipboard By pressing <b>Ctrl</b> key <b>during</b> the drawing process, you can:
 . rotate a rectangle or a text area
 . extend and rotate an ellipse
 . curve a line (cubic Bezier curve) Center click Change font family (generic name) Change font style Change font weight Change linecap Change linejoin Change the style Color Ctrl key Ctrl+1...9 Dashed Dashed line Decrement line width Decrement line width even more Disable on-screen notifications Disable panel indicator Draw Draw On Your Screen becomes Draw On Your Desktop Drawing on the desktop Ellipse Enter/leave drawing mode Entering drawing mode Erase Erase all drawings Erase last brushstroke Escape key Fill Free drawing Full line Global Hide panel and dock Increment line width Increment line width even more Increment/decrement line width Internal Leave Leaving drawing mode Left click Line Menu Open stylesheet.css Persistent Persistent drawing through session restart Preferences Press Ctrl + F1 for help Rectangle Redo Redo last brushstroke Right click Save drawing as a SVG file Screenshot Screenshot to clipboard Scroll See stylesheet.css Select color Select ellipse Select eraser Select line Select rectangle Select text Shift key held Show help Smooth Smooth last brushstroke Smooth stroke during the drawing process Square drawing area Start drawing with Super+Alt+D and save your beautiful work by taking a screenshot Stroke System Text Toggle fill/stroke Transform shape (when drawing) Translators Type your text
and press Enter Undo Undo last brushstroke Unselect shape (free drawing) Version %d You can also smooth the stroke afterward
See “%s” Project-Id-Version: Draw On Your Screen VERSION
Report-Msgid-Bugs-To: https://framagit.org/abakkk/DrawOnYourScreen/issues
PO-Revision-Date: 2019-06-15 09:52+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
Last-Translator: albano battistella <albano_battistella@hotmail.com>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: it_IT
 (in modalità disegno) <u> Nota </ u>: quando salvi gli elementi creati con <b> gomma da cancellare </ b> in un file <b> SVG </ b>,
questi sono colorati con il colore di sfondo, trasparente se disabilitati.
(Vedi "% s" o modifica il file SVG in seguito) Informazioni su Aggiungi uno sfondo di disegno Area screenshot Area screenshot negli appunti Premendo il tasto <b> Ctrl </ b> <b> durante </ b> del processo di disegno, puoi:
  . ruotare un rettangolo o un'area di testo
  . estendere e ruotare un'ellisse
  . curvare una linea (curva Bezier cubica) clic centro Cambia la famiglia del font (nome generico) Cambia stile del font Cambiare spessore del font Cambia il limite di riga Cambia linejoin Cambia lo stile Colore Tasto Ctrl Ctrl+1...9 tratteggiato Linea tratteggiata Decrementa la larghezza della linea Decrementa la larghezza della linea ancora di più Disabilita le notifiche sullo schermo Disabilita indicatore del pannello Disegna Draw On Your Screen diventa Draw On Your Desktop Disegno sul desktop Ellisse Entra/esci dalla modalità disegno Entra in  modalità disegno Cancella Cancella tutti i disegni Cancella l'ultima pennellata tasto Esc Riempi Disegno libero Linea piena Globale Nascondi pannello e dock Incrementa la larghezza della linea Aumentare la larghezza della linea ancora di più Incrementa/decrementa larghezza della linea Interno Lascia Lascia la modalità di disegno Clic sinistro Linea Menu Apri stylesheet.css Persistente Disegno persistente attraverso il riavvio della sessione Preferenze Premi Ctrl + F1 per aiuto Rettangolo Ripeti Ripeti l'ultima pennellata Clic destro Salva disegno come file SVG Screenshot Screenshot negli appunti Scorri Vedi stylesheet.css Seleziona colore Seleziona l'ellisse Seleziona gomma Seleziona la linea Seleziona il rettangolo Seleziona il testo Tasto MAIUSC premuto Mostra aiuto Liscio Liscia l'ultima pennellata Colpo liscio durante il processo di disegno Area di disegno quadrata Inizia a disegnare con Super + Alt + D e salva il tuo bellissimo lavoro scattando uno screenshot Tratto Sistema Testo Attiva / disattiva riempimento / tratto Trasforma la forma (quando si disegna) Albano Battistella Digita il tuo testo
e premere Invio Annulla Annulla l'ultima pennellata Deseleziona forma (disegno libero) Versione %d Puoi anche lisciare il tratto in seguito
Vedi "% s" 